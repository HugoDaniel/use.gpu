declare module "@use-gpu/wgsl/transform/spherical.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getSphericalPosition: ParsedBundle;
  export default __module;
}
