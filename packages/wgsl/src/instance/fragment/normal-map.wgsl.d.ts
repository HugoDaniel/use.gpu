declare module "@use-gpu/wgsl/instance/fragment/normal-map.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getNormalMapFragment: ParsedBundle;
  export default __module;
}
