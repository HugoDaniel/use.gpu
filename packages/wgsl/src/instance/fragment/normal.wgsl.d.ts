declare module "@use-gpu/wgsl/instance/fragment/normal.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getNormalFragment: ParsedBundle;
  export default __module;
}
