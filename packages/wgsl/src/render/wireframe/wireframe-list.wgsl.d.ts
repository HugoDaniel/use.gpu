declare module "@use-gpu/wgsl/render/wireframe/wireframe-list.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getWireframeListVertex: ParsedBundle;
  export default __module;
}
