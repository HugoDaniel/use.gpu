declare module "@use-gpu/wgsl/render/wireframe/wireframe-strip.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getWireframeStripVertex: ParsedBundle;
  export default __module;
}
