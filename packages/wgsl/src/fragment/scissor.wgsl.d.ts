declare module "@use-gpu/wgsl/fragment/scissor.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const scissorFragment: ParsedBundle;
  export const isScissored: ParsedBundle;
  export default __module;
}
